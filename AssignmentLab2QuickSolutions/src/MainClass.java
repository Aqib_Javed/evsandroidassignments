
/**
 * @author AQIB JAVED
 * @since 18-08-2018
 */
public class MainClass {

	// Hi All,
	// Assignment for this week,
	// Assignment 1:
	// A bunch of bots just arrived to earth and looking for some places to live
	//
	// int [][] places = {
	// {2,3,-1},
	// {-1,2,3},
	// {2,3,4}
	// };
	// Bots wants a software which calculate the prices of those room, the values of
	// array is basically the prices of those rooms.
	//
	// Now the room with value -1 are free but they are haunted and bots don't want
	// to live in haunted room in fact bots also do not want to choose room which
	// are below to the haunted rooms
	// so calculate total amount of room.
	// the expected output of above example will be = 2+3+2+3 = 10
	//
	// array data can be dynamic.
	// Make sure result Time complexity should be not exceed O(n)

	/**
	 * @param places
	 * @see {@link MainClass#getTotalRentForAllRooms(Integer[][])}
	 * @return {@link Integer}
	 */
	public static int getTotalRentForAllRooms(int[][] places) {
		long start = System.currentTimeMillis();
		int total = 0;
		for(int row=0;row<places.length;row++) {
			for(int col=0; col< places[row].length;col++) {
				if(places[col][row] == -1) {
					break;
				}else {
					total += places[col][row];
				}
			}
		}
		long end = System.currentTimeMillis();
		System.out.println(((end - start)/1000) +" seconds passed");
		return total;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		System.out.println(getTotalRentForAllRooms(new int[][] { { 2,  3, -1 }, 
																 {-1,  2,  3 }, 
																 { 2,  3,  4 }}));
	}

}
