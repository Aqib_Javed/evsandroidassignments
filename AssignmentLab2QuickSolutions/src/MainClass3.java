/**
 * @author AQIB JAVED
 * @since 18-08-2018
 */
public class MainClass3 {
	// Assignment 3:
	// iven an array of integers, find the pair of adjacent elements that has the
	// largest product and return that product.
	//
	// Example
	// For inputArray = [3, 6, -2, -5, 7, 3], the output should be
	// adjacentElementsProduct(inputArray) = 21.
	// 7 and 3 produce the largest product.
	// Input/Output
	// [execution time limit] 3 seconds (java)
	// [input] array.integer inputArray
	// An array of integers containing at least two elements.
	// Guaranteed constraints:
	// 2 ≤ inputArray.length ≤ 10,
	// -1000 ≤ inputArray[i] ≤ 1000.
	// [output] integer
	// The largest product of adjacent elements.
	/**
	 * @param inputArr
	 * @see {@link MainClass3#getAdjacentElementsLargestProduct(Integer[])}
	 * @return {@link Integer}
	 */
	public static int getAdjacentElementsLargestProduct(int[] inputArr) {
		int largestProduct = Integer.MIN_VALUE;
		for (int x = 1; x < inputArr.length; x++)
			if (inputArr[x - 1] * inputArr[x] > largestProduct)
				largestProduct = inputArr[x - 1] * inputArr[x];
		return largestProduct;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(getAdjacentElementsLargestProduct(new int[] { 3, 6, -2, -5, 7, 3 }));
	}

}
