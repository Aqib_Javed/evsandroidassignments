
public class MainClass4 {
	// Assignment 4:
	// Below we will define an n-interesting polygon.
	// Your task is to find the area of a polygon for a given n.
	//
	// A 1-interesting polygon is just a square with a side of length 1.
	// An n-interesting polygon is obtained by taking the n-1
	// interesting polygon and appending
	// 1-interesting polygons to its rim, side by side.
	// You can see the 1-, 2-, 3- and 4-interesting polygons in the
	// picture which is attached.
	//
	// Example
	// For n = 2, the output should be
	// shapeArea(n) = 5;
	// For n = 3, the output should be
	// shapeArea(n) = 13.
	// Input/Output
	// [execution time limit] 3 seconds (java)
	// [input] integer n
	// Guaranteed constraints:
	// 1 ≤ n < 104.
	// [output] integer
	// The area of the n-interesting polygon.
	// Reference image for this assignment is attached
	public static void main(String[] args) {
		System.out.println(shapeAreaExperts(2));
	}

	// Here is the solution for experts
	// input grid => output grid
	/*
	 * x-> 1 = 1 x-> 2 = 5 => 4(2-1) + 1 x-> 3 = 13 => 4(3-1) + 4(2-1) + 1 x-> 4 =
	 * 4(4-1) + 4(3-1) + 4(2-1) + 1
	 */
	public static int shapeAreaExperts(int n) {
		return (n == 1) ? 1 : 4 *--n + shapeAreaExperts(n--);
	}

	// Here is the solution 1 for beginners
	// Beginner should follow this solution even its
	// looks messy and long
	// but its easy to understand
	/**
	 * @param n
	 * @return
	 */
	public static int shapeArea(int n) {
		int shapeArea = 1;
		if (n == 1)
			return 1;
		shapeArea += shapeAreaLeftPoly(n);
		shapeArea += shapeAreaUpPoly(n);
		shapeArea += shapeAreaRightPoly(n);
		shapeArea += shapeAreaDownPoly(n);
		return shapeArea;
	}

	/**
	 * @param n
	 * @return
	 */
	public static int shapeAreaLeft(int n) {
		if (n == 1)
			return 1;
		return 1 + shapeAreaLeft(n - 1);
	}

	/**
	 * @param n
	 * @return
	 */
	public static int shapeAreaRight(int n) {
		if (n == 1)
			return 1;
		return 1 + shapeAreaRight(n - 1);
	}

	/**
	 * @param n
	 * @return
	 */
	public static int shapeAreaUp(int n) {
		if (n == 1)
			return 1;
		return 1 + shapeAreaUp(n - 1);
	}

	/**
	 * @param n
	 * @return
	 */
	public static int shapeAreaDown(int n) {
		if (n == 1)
			return 1;
		return 1 + shapeAreaDown(n - 1);
	}

	/**
	 * @param n
	 * @return
	 */
	public static int shapeAreaLeftPoly(int n) {
		int shapeArea = 0;
		shapeArea += shapeAreaLeft(n - 1);
		for (int x = 2; x < n;) {
			n--;
			shapeArea += shapeAreaUp(n - 1);
			shapeArea += shapeAreaDown(n - 1);

		}

		return shapeArea;
	}

	/**
	 * @param n
	 * @return
	 */
	public static int shapeAreaRightPoly(int n) {
		int shapeArea = 0;
		shapeArea += shapeAreaRight(n - 1);
		for (int x = 2; x < n;) {
			n--;
			shapeArea += shapeAreaUp(n - 1);
			shapeArea += shapeAreaDown(n - 1);

		}
		return shapeArea;
	}

	/**
	 * @param n
	 * @return
	 */
	public static int shapeAreaUpPoly(int n) {
		int shapeArea = 0;
		shapeArea += shapeAreaUp(n - 1);
		return shapeArea;
	}

	/**
	 * @param n
	 * @return
	 */
	public static int shapeAreaDownPoly(int n) {
		int shapeArea = 0;
		shapeArea += shapeAreaDown(n - 1);
		return shapeArea;
	}

}
